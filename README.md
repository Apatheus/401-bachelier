Readme
==

Ce projet React fait partie intégrante de l'atelier SAE 401, qui consiste à développer une application web utilisant une API. 

L'application que je souhaite créer doit servir à faire le lien entre des données contenant des informations sur le nombre de logements étudiants dans les villes des Hauts-de-Seine ainsi que données sur le nombre d'étudiants dans ces mêmes villes, afin de définir quelles villes des Hauts-de-Seines sont les plus susceptibles d'avoir des logements étudiants de libre.

Les objectifs principaux de la facette React du projet sont décrits ci-dessous.

## Créer une carte interactive
La carte interactive doit permettre d'afficher et sélectionner les villes principales de la région Hauts-de-Seine et d'en afficher le nombre d'étudiants ainsi que le nombre d'appartements étudiants.

## Mettre en place un formulaire de connexion
Ce formulaire doit permettre la connexion d'un utilisateur depuis la page connexion du site, afin qu'il puisse modifier les données de l'API.