import { useState, useEffect } from "react";

const useSessionStorage = (key, initialValue = null) => {
    const [storedValue, setStoredValue] = useState(() => {
        const item = sessionStorage.getItem(key);
        return item ? JSON.parse(item) : initialValue;
    });

    useEffect(() => {
        const handleStorageChange = (e) => {
            if (e.key === key) {
                setStoredValue(e.newValue ? JSON.parse(e.newValue) : initialValue);
            }
        };

        window.addEventListener("storage", handleStorageChange);
        return () => {
            window.removeEventListener("storage", handleStorageChange);
        };
    }, [key, initialValue, storedValue]);

    const setValue = (value) => {
        setStoredValue(value);
        if (value === null) {
            sessionStorage.removeItem(key);
        } else {
            sessionStorage.setItem(key, JSON.stringify(value));
        }
    };

    return [storedValue, setValue];
};

export default useSessionStorage;
