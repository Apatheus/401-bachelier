import React from "react";

const AuthContext = React.createContext({
    isLoggedIn: false,
    setIsLoggedIn: () => { },
});

export const AuthProvider = ({ children, isLoggedIn, setIsLoggedIn }) => {
    return (
        <AuthContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
            {children}
        </AuthContext.Provider>
    );
};

export default AuthContext;
