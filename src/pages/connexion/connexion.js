import * as React from "react";
import { useState } from "react";
import { Icon } from "@iconify/react";
import { useNavigate } from "react-router-dom";
import AuthContext from "../../components/../AuthContext";
import "./connexion.scss";

function Connexion() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();
    const { isLoggedIn, setIsLoggedIn } = React.useContext(AuthContext);

    const handleSubmit = (event) => {
        event.preventDefault();
        const requestOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ username: username, password: password }),
        };
        fetch("http://127.0.0.1:8000/login", requestOptions)
            .then((response) => {
                if (response.status === 200) {
                    return response.text();
                } else {
                    return response.text().then((text) => {
                        throw new Error(text);
                    });
                }
            })
            .then((responseText) => {
                console.log("Response text:", responseText); // Add this line to print the response text
                try {
                    const data = JSON.parse(responseText);
                    const token = data.token;
                    if (token) {
                        sessionStorage.setItem("token", token);
                        setIsLoggedIn(true);
                        navigate("/");
                    } else {
                        console.error("Invalid token:", token);
                    }
                } catch (error) {
                    console.error("Error parsing JSON:", error);
                }
            })
    };

    return (
        <div className="divForm">
            <form onSubmit={handleSubmit}>
                <h1>Connexion</h1>
                <div>
                    <label htmlFor="username">
                        <Icon icon="mdi:user" /> Identifiant :
                    </label>
                    <input
                        type="text"
                        id="username"
                        value={username}
                        onChange={(event) => setUsername(event.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="password">
                        <Icon icon="carbon:password" /> Mot de passe :
                    </label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(event) => setPassword(event.target.value)}
                    />
                </div>
                <button type="submit">Connexion</button>
            </form>
        </div>
    );
}

export default Connexion;
