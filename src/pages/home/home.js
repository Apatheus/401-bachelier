import * as React from "react";
import { useState, useEffect } from "react";
import { MapContainer, TileLayer, Marker, Popup, GeoJSON } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.webpack.css'; // Re-uses images from ~leaflet package
import 'leaflet-defaulticon-compatibility';
import { Icon } from '@iconify/react';
import "./home.scss";

function Home() {
    // La partie qui suit permet de récupérer les données de mon API.
    const [data, setData] = useState({ logements: [], etudiants: [] });

    useEffect(() => {
        fetch("http://127.0.0.1:8000/data")
            .then(response => response.json())
            .then(response => {
                if (response && response.logements && response.etudiants) {
                    setData({ logements: response.logements, etudiants: response.etudiants });
                } else {
                    console.log('Le format de la réponse est invalide.');
                }
                console.log(response.logements);
                console.log(response.etudiants);
            })
            .catch(e => console.log(e))
    }, []);

    // Les coordonnées des communes des Hauts-de-Seine ne sont pas stockées dans mon API, je les ai donc codées manuellement.
    let markers = [
        { id: 1, commune: 'Clichy', position: [48.904526, 2.304768] },
        { id: 2, commune: 'Nanterre', position: [48.892423, 2.215331] },
        { id: 3, commune: 'Bagneux', position: [48.796696, 2.310020] },
        { id: 4, commune: 'Colombes', position: [48.917402, 2.269675] },
        { id: 5, commune: 'Boulogne-Billancourt', position: [48.839695, 2.239912] },
        { id: 6, commune: 'Clamart', position: [48.799815, 2.257289] },
        { id: 7, commune: 'Neuilly-sur-Seine', position: [48.884831, 2.268510] },
        { id: 8, commune: 'Courbevoie', position: [48.900552, 2.259290] },
        { id: 9, commune: 'Levallois-Perret', position: [48.893217, 2.287864] },
        { id: 10, commune: 'Sceaux', position: [48.778016, 2.295092] },
        { id: 11, commune: 'Sèvres', position: [48.821241, 2.210977] },
        { id: 12, commune: 'Vanves', position: [48.822484, 2.288405] },
        { id: 13, commune: 'Antony', position: [48.759255, 2.302553] },
        { id: 14, commune: 'Montrouge', position: [48.816363, 2.317384] },
        { id: 15, commune: 'Suresnes', position: [48.869798, 2.219033] },
        { id: 16, commune: 'Bourg-la-Reine', position: [48.780454, 2.316374] },
        { id: 17, commune: 'Châtillon', position: [48.804689, 2.289387] },
        { id: 18, commune: 'Fontenay-sous-Bois', position: [48.789776, 2.287181] },
        { id: 19, commune: 'Gennevilliers', position: [48.925525, 2.293275] },
        { id: 20, commune: 'Malakoff', position: [48.817275, 2.297760] },
        { id: 21, commune: 'Meudon', position: [48.812995, 2.238470] },
        { id: 22, commune: 'Rueil-Malmaison', position: [48.882767, 2.176930] },
        { id: 23, commune: 'Saint-Cloud', position: [48.847647, 2.208115] },
        { id: 24, commune: 'Vaucresson', position: [48.843890, 2.162447] }
    ];

    // La partie qui suit permet de récupérer et d'afficher les bordures de la région Hauts-de-Seine.
    const [geoJSON, setGeoJSON] = useState(null);
    const fetchGeoJSON = () => {
        fetch(
            "https://france-geojson.gregoiredavid.fr/repo/departements/92-hauts-de-seine/departement-92-hauts-de-seine.geojson"
        )
            .then((resp) => resp.json())
            .then((data) => {
                setGeoJSON(data);
            });
    };

    useEffect(() => {
        fetchGeoJSON();
    }, []);

    // On définit la couleur des bordures de la région Hauts-de-Seine sur la carte Leaflet.
    function style(coordinates) {
        return {
            fillColor: 'black',
            color: 'black',
            weight: 3,
            opacity: 1,
            fillOpacity: .1
        };
    }

    // On modifie la couleur de l'indice de population en fonction de sa valeur.
    function getColor(indice) {
        const minIndice = 0;
        const maxIndice = 100;
        const percentage = Math.max(Math.min((indice - minIndice) / (maxIndice - minIndice), 1), 0);

        const red = Math.round(255 * percentage);
        const green = Math.round(255 * (1 - percentage));
        return `rgb(${red}, ${green}, 0)`;
    }


    return (
        <>
            <link
                rel="stylesheet"
                href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
                integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
                crossorigin=""
            />


            <header>
                <div className="intro">
                    <span>SeineLogements :</span> <br />
                    Les appartements étudiants des <span>Hauts-de-Seine</span><br /><br />
                </div>
                <div className="explications">
                    <div className="explication1">
                        <div className="exTitre">
                            <h3>Trouvez la ville de vos rêves !</h3>
                        </div>
                        <div className="exIcone">
                            <Icon className="icone" icon="mdi:home-city" />
                        </div>
                        <div className="exTexte">
                            <p>Grâce à la carte interactive et son système d'indice de population, trouvez la ville étudiante qui vous convient le mieux.</p>
                        </div>
                    </div>
                    <div className="explication2">
                        <div className="exTitre">
                            <h3>Des données interactives</h3>
                        </div>
                        <div className="exIcone">
                            <Icon className="icone" icon="mdi:database-search-outline" />
                        </div>
                        <div className="exTexte">
                            <p>Stockées dans une API, les données du site sont parfaitement interactives. <br /> Rendez-vous sur la page connexion !</p>
                        </div>
                    </div>
                </div>
            </header>
            <main>
                <div className="section-carte">
                    <div className="carte">
                        <MapContainer center={[48.837, 2.235]} zoom={11} scrollWheelZoom={false}>
                            <TileLayer
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            />
                            {geoJSON && <GeoJSON data={geoJSON} style={style} />}
                            {markers.map(marker => (
                                <Marker key={marker.id} position={marker.position} className="marker">
                                    <Popup className="popup">
                                        <div>
                                            <h2>{marker.commune}</h2>
                                            <p>
                                                Nombre d'étudiants : {data.etudiants.find(e => e.geoNom?.toLowerCase() === marker.commune.toLowerCase())?.nbEtudiants}<br />
                                                Appartements étudiants : {data.logements.find(l => l.commune?.toLowerCase() === marker.commune.toLowerCase())?.nbLogementsEtudiants}
                                            </p>
                                            <p style={{ color: getColor((data.etudiants.find(e => e.geoNom?.toLowerCase() === marker.commune.toLowerCase())?.nbEtudiants / data.logements.find(l => l.commune?.toLowerCase() === marker.commune.toLowerCase())?.nbLogementsEtudiants).toFixed(0)) }}>
                                                Indice de population : {(data.etudiants.find(e => e.geoNom?.toLowerCase() === marker.commune.toLowerCase())?.nbEtudiants / data.logements.find(l => l.commune?.toLowerCase() === marker.commune.toLowerCase())?.nbLogementsEtudiants).toFixed(0)}<br />
                                            </p>
                                        </div>
                                    </Popup>
                                </Marker>
                            ))}

                        </MapContainer>
                    </div>
                </div>
            </main>

        </>
    )
}
export default Home;