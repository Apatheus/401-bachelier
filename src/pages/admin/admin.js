import React, { useState, useEffect } from "react";
import EditForm from "./../../components/editForm/EditForm";
import Pagination from "react-js-pagination";
import './admin.scss';

function Admin() {
    const [data, setData] = useState({ etudiants: [], logements: [] });
    const [editingItem, setEditingItem] = useState(null);
    const [editingType, setEditingType] = useState(null);
    const [activePageEtudiants, setActivePageEtudiants] = useState(1);
    const [activePageLogements, setActivePageLogements] = useState(1);
    const itemsPerPage = 10;

    useEffect(() => {
        // Récupère les données de mon API
        fetch("http://127.0.0.1:8000/data")
            .then((response) => response.json())
            .then((jsonData) => setData(jsonData));
    }, []);

    const handleEdit = (item, type) => {
        setEditingItem(item);
        setEditingType(type);
    };

    const handleCancelEdit = () => {
        setEditingItem(null);
        setEditingType(null);
    };

    const handleUpdate = async (updatedItem, type) => {
        const updateEndpoint = type === "etudiants" ? "etudiants/update" : "logements/update";

        const requestOptions = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": sessionStorage.getItem("token"),
            },
            body: JSON.stringify(updatedItem),
        };

        try {
            const response = await fetch(`http://127.0.0.1:8000/${updateEndpoint}`, requestOptions);

            if (!response.ok) {
                throw new Error(`Error while updating item: ${response.statusText}`);
            }

            const contentType = response.headers.get("content-type");
            if (!contentType || !contentType.includes("application/json")) {
                throw new Error("Received response is not valid JSON");
            }

            const jsonData = await response.json();

            if (type === "etudiants") {
                setData({
                    ...data,
                    etudiants: data.etudiants.map((item) =>
                        item.geoNom === jsonData.geoNom ? jsonData : item
                    ),
                });
            } else {
                setData({
                    ...data,
                    logements: data.logements.map((item) =>
                        item.commune === jsonData.commune ? jsonData : item
                    ),
                });
            }
            setEditingItem(null);
            setEditingType(null);
        } catch (error) {
            console.error("Error while updating item:", error);
        }
    };

    const renderPaginatedList = (list, activePage) => {
        const startIndex = (activePage - 1) * itemsPerPage;
        const endIndex = Math.min(startIndex + itemsPerPage, list.length);

        return list.slice(startIndex, endIndex);
    };

    return (
        <div className="divGlobale">
            <h1>Admin</h1>
            {editingItem ? (
                <EditForm
                    item={editingItem}
                    type={editingType}
                    onUpdate={handleUpdate}
                    onCancel={handleCancelEdit}
                />
            ) : (
                <div className="tables-container">
                    <div className="table-section">
                        <h2>Etudiants</h2>
                        <div className="card">
                            <table>
                                <thead>
                                    <tr>
                                        <th>GeoNom</th>
                                        <th>NbEtudiants</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {renderPaginatedList(data.etudiants, activePageEtudiants).map(
                                        (item) => (
                                            <tr key={item.geoNom}>
                                                <td>{item.geoNom}</td>
                                                <td>{item.nbEtudiants}</td>
                                                <td>
                                                    <button onClick={() => handleEdit(item, "etudiants")}>
                                                        Modifier
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    )}
                                </tbody>
                            </table>
                            <Pagination
                                activePage={activePageEtudiants}
                                itemsCountPerPage={itemsPerPage}
                                totalItemsCount={data.etudiants.length}
                                pageRangeDisplayed={5}
                                onChange={setActivePageEtudiants}
                            />
                        </div>
                    </div>
                    <div className="table-section">
                        <h2>Logements</h2>
                        <div className="card">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Commune</th>
                                        <th>NbLogementsEtudiants</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {renderPaginatedList(data.logements, activePageLogements).map(
                                        (item) => (
                                            <tr key={item.commune}>
                                                <td>{item.commune}</td>
                                                <td>{item.nbLogementsEtudiants}</td>
                                                <td>
                                                    <button onClick={() => handleEdit(item, "logements")}>
                                                        Modifier
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    )}
                                </tbody>
                            </table>
                            <Pagination
                                activePage={activePageLogements}
                                itemsCountPerPage={itemsPerPage}
                                totalItemsCount={data.logements.length}
                                pageRangeDisplayed={5}
                                onChange={setActivePageLogements}
                            />
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}

export default Admin;