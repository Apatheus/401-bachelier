import * as React from "react";
import Navbar from "./navbar/navbar";
import Footer from "./footer/footer";
import { Outlet } from "react-router-dom";
import AuthContext from "../AuthContext";

function Layout() {
    const { isLoggedIn, setIsLoggedIn } = React.useContext(AuthContext);

    // Update the isLoggedIn state whenever its value changes
    React.useEffect(() => {
        const token = sessionStorage.getItem("token");
        setIsLoggedIn(!!token);
    }, []);

    return (
        <body>
            <Navbar />
            <Outlet />
            <Footer />
        </body>
    );
}

export default Layout;
