import * as React from "react";
import { Link } from "react-router-dom";
import styles from "./footer.module.scss";
import { Icon } from '@iconify/react';

function Footer() {

    return (
        <>
            <footer>
                <div className={styles.signature}>
                    © Damien Bachelier, 2023
                </div>
                <div className={styles.liensFooter}>
                    <Link to='/' className={styles.lien}>Accueil</Link>
                    <Link to='connexion' className={styles.lien}>Connexion</Link>
                </div>
                <div className={styles.socials}>
                    <a href="linkedin.com/in/damien-b-434793241/"><Icon className={styles.footerIcon} icon="mdi:linkedin" /></a>
                    <a href="https://twitter.com/suehtapa"><Icon className={styles.footerIcon} icon="mdi:twitter" /></a>
                    <a href=""><Icon className={styles.footerIcon} icon="mdi:link-box-variant" /></a>
                </div>
            </footer>
        </>
    )
}

export default Footer;