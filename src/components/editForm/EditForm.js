import React, { useState } from "react";

function EditForm({ item, type, onUpdate, onCancel }) {
    const [updatedItem, setUpdatedItem] = useState(item);

    const handleChange = (event) => {
        const { name, value } = event.target;
        setUpdatedItem({ ...updatedItem, [name]: value });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        onUpdate(updatedItem, type);
    };

    return (
        <form onSubmit={handleSubmit}>
            <h2>Modifier {type === "etudiants" ? "Etudiant" : "Logement"}</h2>
            {type === "etudiants" ? (
                <>
                    <label htmlFor="geoNom">GeoNom:</label>
                    <input
                        type="text"
                        id="geoNom"
                        name="geoNom"
                        value={updatedItem.geoNom}
                        onChange={handleChange}
                    />
                    <label htmlFor="nbEtudiants">NbEtudiants:</label>
                    <input
                        type="number"
                        id="nbEtudiants"
                        name="nbEtudiants"
                        value={updatedItem.nbEtudiants}
                        onChange={handleChange}
                    />
                </>
            ) : (
                <>
                    <label htmlFor="commune">Commune:</label>
                    <input
                        type="text"
                        id="commune"
                        name="commune"
                        value={updatedItem.commune}
                        onChange={handleChange}
                    />
                    <label htmlFor="nbLogementsEtudiants">NbLogementsEtudiants:</label>
                    <input
                        type="number"
                        id="nbLogementsEtudiants"
                        name="nbLogementsEtudiants"
                        value={updatedItem.nbLogementsEtudiants}
                        onChange={handleChange}
                    />
                </>
            )}
            <button type="submit">Sauvegarder</button>
            <button type="button" onClick={onCancel}>
                Annuler
            </button>
        </form>
    );
}

export default EditForm;
