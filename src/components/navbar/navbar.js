import * as React from "react";
import { Link, useNavigate } from "react-router-dom";
import styles from "./navbar.module.scss";
import ToggleSwitch from "react-toggle-switch";
import "react-toggle-switch/dist/css/switch.min.css";
import AuthContext from "../../AuthContext";

function Navbar() {
    const { isLoggedIn, setIsLoggedIn } = React.useContext(AuthContext);
    const navigate = useNavigate();

    const handleLogout = () => {
        sessionStorage.removeItem("token");
        setIsLoggedIn(false);
        navigate("/connexion");
    };

    return (
        <>
            <nav className={styles.navbar}>
                <Link to="/">
                    <a className={styles.logo}>SeineLogements</a>
                </Link>
                <div className={styles.liens}>
                    <ul className={styles.navmenu}>
                        <li>
                            <Link to="/" className={styles.navhover}>
                                Accueil
                            </Link>
                        </li>
                        {isLoggedIn && (
                            <li>
                                <Link to="/admin" className={styles.navhover}>
                                    Admin
                                </Link>
                            </li>
                        )}
                        {isLoggedIn ? (
                            <li onClick={handleLogout} className={styles.bouton}>
                                Se déconnecter
                            </li>
                        ) : (
                            <li>
                                <Link to="/connexion" className={styles.bouton}>
                                    Connexion
                                </Link>
                            </li>
                        )}
                    </ul>
                </div>
            </nav>
        </>
    );
}

export default Navbar;
