import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import { Routes, Route, BrowserRouter, Navigate } from "react-router-dom";
import Layout from './components/layout';
import Home from './pages/home/home';
import Connexion from './pages/connexion/connexion';
import Admin from './pages/admin/admin';
import { AuthProvider } from './components/../AuthContext';
import './assets/scss/style.scss';

function Index() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  return (
    <React.StrictMode>
      <AuthProvider isLoggedIn={isLoggedIn} setIsLoggedIn={setIsLoggedIn}>
        <BrowserRouter>
          <Routes>
            <Route element={<Layout />}>
              <Route path="/" element={<Home />} />
              <Route path="connexion" element={<Connexion />} />
              <Route path="/admin" element={<Admin />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </AuthProvider>
    </React.StrictMode>
  );
}

ReactDOM.render(<Index />, document.getElementById('root'));

reportWebVitals();
